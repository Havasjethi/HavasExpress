export * from './classes';
export * from './decorators/index';
export * from './decorators/parameter_decorator_storage';
export * from './di';
export * from './types';
export { AfterCreate, BeforeCreate, extender, SetProperty } from './util/class_decorator_util';

import {
  NextFunction,
  ParamsDictionary, Request as Req, Response as Res
} from 'express-serve-static-core';
import { ParsedQs } from 'qs';

export type Response = Res;
export type Request = Req;
export type Next = NextFunction;

export type ExpressRequest = Req<ParamsDictionary, any, any, ParsedQs, Record<string, any>>;
export type ExpressResponse = Res<any, Record<string, any>, number>;
export type ExpressNext = NextFunction;
