import { ExpressCoreRoutable } from '../classes';
import { Middleware } from '../types/classes/middleware';
import { ExpressHttpMethod } from '../types/native_http_methods';
import { SetProperty } from '../util';

export interface MiddlewareEntry {
  method: ExpressHttpMethod;
  path: string;
}

export function UseMiddleware(...middlewares: Middleware[]) {
  return SetProperty<ExpressCoreRoutable<any>>((created_element) => {
    created_element.add_constructor_middleware({ path: '/', middlewares });
  });
}

export function MethodSpecificMiddlewares(method: ExpressHttpMethod, ...middlewares: Middleware[]) {
  return SetProperty<ExpressCoreRoutable<any>>((created_element) => {
    created_element.add_constructor_middleware({ method, path: '/', middlewares });
  });
}

/**
 * @deprecated
 */
export function ComplexMiddleware({ method, path }: MiddlewareEntry, ...middlewares: Middleware[]) {
  return SetProperty<ExpressCoreRoutable<any>>((created_element) => {
    created_element.add_constructor_middleware({ method, path, middlewares });
  });
}
