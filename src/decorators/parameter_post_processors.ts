import { postProcessorStorage } from '../classes/post_processor_storage';
import {
  DynamicProcessorFunction,
  ProcessorFactoryToken,
  ProcessorFunction,
  ProcessorToken,
} from '../types';

/**
 * Creates a token, which will be used for returning the post processor function at runtime.
 * @param {ProcessorFunction<Args, Result>} processorFunction<Args, Result>
 * @returns {ProcessorToken}
 */
export function createPostProcessor<Args = any, Result = any>(
  processorFunction: ProcessorFunction<Args, Result>,
): ProcessorToken {
  return {
    token: postProcessorStorage.registerPostProcessor(processorFunction),
  };
}

export function createPostProcessorFactory<FactoryArgs = unknown>(
  processorFactoryFunction: DynamicProcessorFunction<FactoryArgs>,
): (args: FactoryArgs) => ProcessorFactoryToken {
  const token = postProcessorStorage.registerPostProcessorFactory(processorFactoryFunction);

  return (args: FactoryArgs) => {
    return { token, args };
  };
}
