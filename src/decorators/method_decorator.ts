import { Middleware } from '../types/classes/middleware';
import { ExpressHttpMethod } from '../types/native_http_methods';
import { ExpressCoreRoutable } from '../classes';
import { extender } from '../util';

function add_endpoint_handler(
  target: ExpressCoreRoutable<any>,
  method_name: string,
  method_type: ExpressHttpMethod,
  path: string,
  middlewares: Middleware[],
) {
  extender.setProperty<ExpressCoreRoutable>(target.constructor.name, (instance) => {
    instance.registerEndpoint(method_name, method_type, path, middlewares);
  });
}

// export function Get (path: string = '/', wrap: boolean, ...middlewares: Middleware[]) {
// export function Get (o: {path: string, wrap: boolean} , ...middlewares: Middleware[]) {
// export function Get (...args: [string, ...Middleware[]] | [string, boolean, ...Middleware[]] ) {
export function Get(path: string = '/', ...middlewares: Middleware[]) {
  return function (
    target: ExpressCoreRoutable<any>,
    propertyKey: string,
    _descriptor: PropertyDescriptor,
  ) {
    add_endpoint_handler(target, propertyKey, 'get', path, middlewares);
  };
}

export function Post(path: string = '/', ...middlewares: Middleware[]) {
  return function (
    target: ExpressCoreRoutable<any>,
    propertyKey: string,
    _descriptor: PropertyDescriptor,
  ) {
    add_endpoint_handler(target, propertyKey, 'post', path, middlewares);
  };
}

export function Delete(path: string = '/', ...middlewares: Middleware[]) {
  return function (
    target: ExpressCoreRoutable<any>,
    propertyKey: string,
    _descriptor: PropertyDescriptor,
  ) {
    add_endpoint_handler(target, propertyKey, 'delete', path, middlewares);
  };
}

export function Put(path: string = '/', ...middlewares: Middleware[]) {
  return function (
    target: ExpressCoreRoutable<any>,
    propertyKey: string,
    _descriptor: PropertyDescriptor,
  ) {
    add_endpoint_handler(target, propertyKey, 'put', path, middlewares);
  };
}

export function Option(path: string = '/', ...middlewares: Middleware[]) {
  return function (
    target: ExpressCoreRoutable<any>,
    propertyKey: string,
    _descriptor: PropertyDescriptor,
  ) {
    add_endpoint_handler(target, propertyKey, 'options', path, middlewares);
  };
}

export function Head(path: string = '/', ...middlewares: Middleware[]) {
  return function (
    target: ExpressCoreRoutable<any>,
    propertyKey: string,
    _descriptor: PropertyDescriptor,
  ) {
    add_endpoint_handler(target, propertyKey, 'head', path, middlewares);
  };
}

export function Patch(path: string = '/', ...middlewares: Middleware[]) {
  return function (
    target: ExpressCoreRoutable<any>,
    propertyKey: string,
    _descriptor: PropertyDescriptor,
  ) {
    add_endpoint_handler(target, propertyKey, 'patch', path, middlewares);
  };
}

export function Method(httpMethod: ExpressHttpMethod, path: string, ...middlewares: Middleware[]) {
  return function (
    target: ExpressCoreRoutable<any>,
    propertyKey: string,
    _descriptor: PropertyDescriptor,
  ) {
    add_endpoint_handler(target, propertyKey, httpMethod, path, middlewares);
  };
}
