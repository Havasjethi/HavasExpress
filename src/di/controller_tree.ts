import { Container, decorate, injectable } from 'inversify';
import { ExpressCoreRoutable } from '../classes/express_core_routable';
import { Constructor } from '../util/class_decorator_util';
import { wrapperConstructorName } from '../util/class_extender';
import { mainContainer } from './container';

export const getName = (target: Constructor<ExpressCoreRoutable>) =>
  target.name !== wrapperConstructorName ? target.name : target.prototype.constructor.name;

export const getWrapper = (target: Constructor<any>) =>
  target.name !== wrapperConstructorName ? target : target.prototype.constructor.name;

class ComposableTreeNode<T> {
  children: ComposableTreeNode<T>[] = [];
  constructor(public value: T) {}

  isPresent(): boolean {
    return this.value !== undefined;
  }

  addChild(x: ComposableTreeNode<T>): this {
    this.children.push(x);
    return this;
  }

  addChildren(x: ComposableTreeNode<T>[]): this {
    this.children.push(...x);
    return this;
  }
}

export class ComposableTreeCreator<T> {
  cachedTree?: Constructor<ComposableTreeNode<T>>;
  protected subNodes: [node: T, parnet: T | undefined][] = [];
  protected mainNode?: T;
  constructor(protected nodeConstructor: Constructor<ComposableTreeNode<T>>) {}

  public registerMainNode(node: T) {
    if (this.mainNode !== undefined) {
      throw new Error('You already declared a mainController!');
    }

    this.mainNode = node;
  }

  public registerNode(node: T, parentNode?: T) {
    // TODO :: Note ::
    // The parentNode might always be registered before the child node,
    // since inorder to refer to the parnet it has to be imported to the file
    // so it will be included before it's child

    this.subNodes.push([node, parentNode]);
  }

  public compose(): ComposableTreeNode<T>[] {
    return this.mainNode ? [this.composeMainTree()] : this.composeForest();
  }

  /**
   * Contains one or more trees
   */
  public getTrees(): ComposableTreeNode<T>[] {
    return this.compose();
  }

  private composeMainTree(): ComposableTreeNode<T> {
    const mainNode = new this.nodeConstructor(this.mainNode);

    const lameLookup: ComposableTreeNode<T>[] = [];
    const nextIteration: [T, T | undefined][] = [];

    for (const [node, parent] of this.subNodes) {
      if (!parent || parent === this.mainNode) {
        const wrappedNode = new this.nodeConstructor(node);
        mainNode.addChild(wrappedNode);
        lameLookup.push(wrappedNode);
      } else {
        nextIteration.push([node, parent]);
      }
    }

    let maxIterations = 100;

    do {
      for (const [node, parent] of nextIteration.splice(0)) {
        const index = lameLookup.findIndex((e) => e.value === parent);

        if (index > 0) {
          const wrappedNode = new this.nodeConstructor(node);

          lameLookup[index].addChild(wrappedNode);
          lameLookup.push(wrappedNode);
        } else {
          nextIteration.push([node, parent]);
        }
      }
    } while (nextIteration.length !== 0 && maxIterations-- > 0);

    if (nextIteration.length !== 0) {
      throw new Error('Cannot insert elements, missing parents!');
    }

    return mainNode;
  }

  private composeForest(): ComposableTreeNode<T>[] {
    const remainingItems = [];
    // const iterationItems = [];

    const lameLookup: [item: ComposableTreeNode<T>, parnet: T][] = [];

    for (const [node, parent] of this.subNodes) {
      if (parent === undefined) {
        // iterationItems.push([node, parent]);
        lameLookup.push([new this.nodeConstructor(node), node]);
      } else {
        remainingItems.push([node, parent]);
      }
    }

    let maxIteration = 100;
    while (remainingItems.length === 0 && maxIteration-- > 0) {
      for (const [node, parent] of remainingItems.splice(0)) {
        const matchingParent = lameLookup.find((e) => e[1] === parent);

        if (matchingParent !== undefined) {
          const wrappedNode = new this.nodeConstructor(node);
          lameLookup.push([wrappedNode, node]);
          matchingParent[0].addChild(wrappedNode);
        } else {
          remainingItems.push([node, parent]);
        }
      }
    }

    return lameLookup.map((e) => e[0]);
  }
}

export class ControllerTreeCreator extends ComposableTreeCreator<Constructor<ExpressCoreRoutable>> {
  constructor() {
    super(ComposableTreeNode);
  }

  public initialize(container: Container): ExpressCoreRoutable[] {
    return this.getTrees().map((e) => this.initializeNode(e, container));
  }

  public initializeNode(
    node: ComposableTreeNode<Constructor<ExpressCoreRoutable>>,
    container: Container,
  ): ExpressCoreRoutable {
    const initializedController = container.get(node.value);

    for (const child of node.children) {
      initializedController.addChild(this.initializeNode(child, container));
    }

    return initializedController;
  }

  public getRegisteredNodes(): Constructor<ExpressCoreRoutable>[] {
    const items: Constructor<ExpressCoreRoutable>[] = [];
    if (this.mainNode) {
      items.push(this.mainNode);
    }

    items.push(...this.subNodes.map((e) => e[0]));

    return items;
  }
}
