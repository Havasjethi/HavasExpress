import { decorate, injectable } from 'inversify';
import { ExpressCoreRoutable } from '../classes';
import { Constructor, OnlyWrap } from '../util';
import { mainContainer, MainControllerTree } from './container';
import 'reflect-metadata';

export const MainController = (target: Constructor<ExpressCoreRoutable>): any => {
  decorate(injectable(), target);
  const wrappedTarget = OnlyWrap(target);
  MainControllerTree.registerMainNode(wrappedTarget as any);
  mainContainer.bind(wrappedTarget).toSelf();

  return wrappedTarget;
};

export const Controller = (parent?: Constructor<ExpressCoreRoutable>): any => {
  return (target: Constructor<ExpressCoreRoutable>) => {
    const wrappedTarget = OnlyWrap(target);
    decorate(injectable(), wrappedTarget);
    MainControllerTree.registerNode(wrappedTarget, parent);
    mainContainer.bind(wrappedTarget).toSelf();

    return wrappedTarget;
  };
};

export const Component = (identifier?: string | symbol) => (target: Constructor<any>) => {
  decorate(injectable(), target);

  mainContainer.bind(identifier ?? target).toSelf();
  // Note :: This should be added if `autoBindInjectable` is set to `false`
  // if (target.length !== 0) {
  //   UnresolvedIdentifierHandler.add(identifier ?? target, target);
  //   console.log('Target::', target);
  //   // mainContainer.bind(identifier ?? target).toSelf();
  // }
};
//
// class UnresolvedHandler {
//   identifiersUnresolved = [];
//   add<T>(identifier: ServiceIdentifier, target: NewableFunction) {
//     // @ts-ignore
//     const reader: MetadataReader = mainContainer._metadataReader;
//     const requiredServiceIdentifiers = reader.getConstructorMetadata(target);
//
//     const x: SiNode = {
//       name: identifier,
//       requires: [],
//       depends: [],
//       ur_handler: this,
//     };
//     // mainContainer.is
//   }
// }
// interface SiNode {
//   name: ServiceIdentifier;
//   requires: ServiceIdentifier[];
//   depends: ServiceIdentifier[];
//   ur_handler: UnresolvedHandler;
// }
//
// export const UnresolvedIdentifierHandler = new UnresolvedHandler();
