import { ErrorRequestHandler, IRouter, NextFunction } from 'express';
import { ErrorHandlerFunction, MapLike, MiddlewareObject, ExpressWebDescription } from '.';
import { mainContainer } from '../di';
import {
  DynamicParameterExtractorFunction,
  ErrorHandlerEntry,
  ExpressEndpoint,
  isProcessorFactory,
  RegistrableMiddleware,
  ResultWrapperFunction,
  StaticParameterExtractorFunction,
  UniversalPostProcessor,
} from '../types';
import { ExpressFunction, Middleware, GeneralExpressFunction } from '../types/classes/middleware';
import { interfaces } from 'inversify';
import ServiceIdentifier = interfaces.ServiceIdentifier;
import { ExpressRequest, ExpressResponse } from '..';
import { ParameterExtractorStorage } from '../decorators';
import { postProcessorStorage } from './post_processor_storage';

export const isPromise = (value: any) => value?.constructor?.name === 'Promise';

export interface WebFrameworkCreator {
  setupLayers(): void;
}

/**
 * TODO :: Initalize with 2 parameters.
 *   1. Description about the endpoint
 *   2. The actual endpont class
 *
 * Note: Curretly the ExpressCoreRoutable is doing this two jobs at the same time
 * this will be refactored in the next commits -> It is going to be possible to
 * create multiple backends for based from the description and the class objects
 */
export class ExpressCreator implements WebFrameworkCreator {
  constructor(
    private havasRouter: ExpressWebDescription,
    private controllerClass: MapLike<any> = havasRouter,
  ) {}

  public setupLayers(router: ExpressWebDescription = this.havasRouter): void {
    this.setupMiddlewares(router.middlewares);
    this.setupMethods(Object.values(router.endpoints));

    // TODO ::
    //  Add this pls: Initialize Result Wrapper Function for each nodes (Router)
    //  this.setupDefaultHandler(this.get_added_methods() as Required<MethodEntry>[]);

    router.children.forEach((child: ExpressWebDescription) => {
      if (!child.layersInitialized) {
        new ExpressCreator(child).setupLayers();
      }

      this.getRoutable().use(child.path, child.getRoutable());
    });

    this.setupDefaultHandler(router);
    this.setupErrorHandlers(router);

    router.layersInitialized = true;
  }

  protected setupDefaultHandler(havasRouter: ExpressWebDescription): void {
    if (havasRouter.defaultHandlerMethod) {
      // TODO :: Create method instead // Note :: This comment might be outdated
      this.getRoutable().use(
        this.methodCreator(havasRouter.defaultHandlerMethod).bind(this.havasRouter),
      );
    } else if (havasRouter.defaultHandlerFunction) {
      this.getRoutable().use(havasRouter.defaultHandlerFunction.bind(this.havasRouter));
    }
  }

  protected setupErrorHandlers(havasRouter: ExpressWebDescription): void {
    const routable = havasRouter.getRoutable();

    Object.values(havasRouter.errorHandlerMethods).forEach((errorHandlerMethod) => {
      const method = this.errorHandlerCreator(errorHandlerMethod).bind(this.havasRouter);
      routable.use(method);
    });

    havasRouter.errorHandlerFunctions.forEach((e) => routable.use(e));
  }

  protected errorHandlerCreator(endpoint: ErrorHandlerEntry): ErrorHandlerFunction {
    // Todo :: Post Processors // Will be implemented in the PostProcessor update
    return !endpoint.parameters || endpoint.parameters.length === 0
      ? (error, request, response, next) =>
          this.getMethodT<ErrorHandlerFunction>(endpoint.methodName).apply(this.havasRouter, [
            error,
            request,
            response,
            next,
          ])
      : // ? this.getMethodT<ErrorHandlerFunction>(endpoint.methodName).bind(this.havasRouter) // , [error, request, response, next])
        async (error, request, response, next): Promise<ErrorRequestHandler> => {
          const parameters = await this.get_parameters(endpoint, request, response, next, error);

          return this.thisBindedInvoke(endpoint.methodName, parameters);
        };
  }

  /** Returns the requested parameters for the endpoint / errorHandler / ?Result wrapper */
  protected async get_parameters(
    endpoint: ErrorHandlerEntry,
    request: ExpressRequest,
    response: ExpressResponse,
    next: NextFunction,
    error?: Error,
    result?: unknown,
  ): Promise<any[]> {
    const parameters: any[] = [];

    for (const parameter of endpoint.parameters.sort((a, b) => a.index! - b.index!)) {
      const { extractor, type } = ParameterExtractorStorage.get_parameter_extractor(parameter.name);
      let preprocessed_value;

      if (type === 'Static') {
        preprocessed_value = (extractor as StaticParameterExtractorFunction)(
          request,
          response,
          next,
          error,
          result,
        );
      } else if (type === 'Dynamic') {
        preprocessed_value = (extractor as DynamicParameterExtractorFunction)(
          parameter.arguments,
          request,
          response,
          next,
        );
      }

      const final_value = parameter.postProcessors
        ? await this.post_process_parameter(parameter.postProcessors, preprocessed_value)
        : preprocessed_value;

      parameters.push(final_value);
    }

    return parameters;
  }

  protected async post_process_parameter<Result = any>(
    post_processors: UniversalPostProcessor[],
    processable: any,
  ): Promise<Result> {
    let processed_value = processable;

    for (const processor of post_processors) {
      let intermediate_value;

      if (typeof processor === 'function') {
        intermediate_value = processor(processable);
      } else if (isProcessorFactory(processor)) {
        const processorFunction = postProcessorStorage.getPostProcessorFactoryDangerous(
          processor.token,
        );
        intermediate_value = processorFunction(processable, processor.args);
      } else {
        const processorFunction = postProcessorStorage.getPostProcessorDangerous(processor.token);
        intermediate_value = processorFunction(processable);
      }

      processed_value = isPromise(intermediate_value)
        ? await intermediate_value
        : intermediate_value;
    }

    return processed_value;
  }

  protected setupMiddlewares(middlewares: RegistrableMiddleware[]): void {
    const routable = this.getRoutable();

    for (const middlewareEntry of middlewares) {
      const functions: GeneralExpressFunction[] = middlewareEntry.middlewares.map((e) =>
        this._middleWareHandlerConverter(e),
      );

      // Adding Converted middleware Function to Express
      middlewareEntry.method !== undefined
        ? routable[middlewareEntry.method](middlewareEntry.path, ...functions)
        : routable.use(middlewareEntry.path, ...functions);
    }
  }

  protected setupMethods(endpoints: ExpressEndpoint[]): void {
    const routable = this.getRoutable();

    for (const endpoint of endpoints) {
      const endpointMiddlewares = endpoint.middlewares.map((middleware: Middleware) =>
        this._middleWareHandlerConverter(middleware),
      );

      routable[endpoint.methodType](
        endpoint.path,
        ...endpointMiddlewares,
        this.methodCreator(endpoint).bind(this.havasRouter),
      );
    }
  }

  protected _middleWareHandlerConverter(middleware: Middleware): GeneralExpressFunction {
    const middlewareHandler = this.getMiddlewareHandler(middleware);
    const isMiddlewareClass = !!middlewareHandler;

    if (isMiddlewareClass) {
      const obj = mainContainer.get(middleware as ServiceIdentifier<MiddlewareObject>);
      return obj.handlerReqest.bind(obj);
    }

    if (typeof middleware === 'function') {
      return middleware as GeneralExpressFunction;
    }

    if (typeof middleware === 'object' && !!middleware.handlerReqest) {
      return (middleware as MiddlewareObject).handlerReqest.bind(middleware);
    }

    console.error('Error :: Middleware called with invalid value', middlewareHandler);
    return (_req: any, _res: any, next: () => void) => {
      next();
    };
  }

  private getMiddlewareHandler(middleware: Middleware) {
    return (
      // @ts-ignore
      middleware?.prototype?.getHandler ||
      // @ts-ignore
      middleware?.prototype?.handle ||
      // @ts-ignore
      middleware?.prototype?.prototype?.getHandler ||
      // @ts-ignore
      middleware?.prototype?.prototype?.handle ||
      // @ts-ignore
      middleware?.prototype?.prototype?.prototype?.getHandler ||
      // @ts-ignore
      middleware?.prototype?.prototype?.prototype?.handle ||
      // @ts-ignore
      middleware?.prototype?.prototype?.prototype?.prototype?.getHandler ||
      // @ts-ignore
      middleware?.prototype?.prototype?.prototype?.prototype?.handle ||
      undefined
    );
  }

  /**
   * Creates callable method for the endpoint
   * TODO :: Minimize overhead // Note: Performance tests are prerequisite
   */
  protected methodCreator(endpoint: ExpressEndpoint): ExpressFunction {
    const methodFunction: ExpressFunction =
      endpoint.parameters === undefined || endpoint.parameters.length === 0
        ? this.getExpressFunction(endpoint.methodName) // TODO :: ??Bind this??
        : async (request, response, next): Promise<ExpressFunction> => {
            const parameters = await this.get_parameters(endpoint, request, response, next);

            return this.thisBindedInvoke(endpoint.methodName, parameters);
          };

    return this.mightWrapFunction(methodFunction);
  }

  /**
   * Wrap function if wrapper defined
   */
  private mightWrapFunction(functionToWrap: ExpressFunction): ExpressFunction {
    const wrapper = this.getResultWrapperFunction();

    return wrapper === undefined
      ? functionToWrap
      : (request: ExpressRequest, response: ExpressResponse, next: NextFunction) => {
          const result = functionToWrap(request, response, next);

          if (response.headersSent) {
            return;
          }
          // What happens if they call the next function
          if (isPromise(result)) {
            result
              .then((result: any) => {
                // TODO :: check `response.headersSent` again
                (wrapper as ResultWrapperFunction)({ result, request, response, next });
              })
              .catch((e: any) => next(e));
          } else {
            (wrapper as ResultWrapperFunction)({ result, request, response, next });
          }
        };
  }

  protected getResultWrapperFunction(): ExpressFunction | ResultWrapperFunction<any> | undefined {
    const wrapperWithClass = this.havasRouter.getResultWrapper();

    if (!wrapperWithClass) {
      return undefined;
    }

    const [wrapper, wrapperClass] = wrapperWithClass;

    if (typeof wrapper === 'function') {
      return wrapper as unknown as ExpressFunction;
    }

    return !wrapper.parameters || wrapper.parameters.length === 0
      ? this.controllerClass[wrapper.methodName].bind(this.controllerClass)
      : ((({ result, request, response, next }) => {
          const parameters = this.get_parameters(
            endpoint,
            request,
            response,
            next,
            undefined,
            result,
          );

          // @ts-ignore
          wrapperClass[wrapper.methodName].bind(wrapperClass)(...parameters);
        }) as ResultWrapperFunction<any>);
  }

  private getRoutable(): IRouter {
    return this.havasRouter.routable;
  }

  private getExpressFunction(methodName: string): ExpressFunction {
    return this.controllerClass[methodName] as ExpressFunction;
  }

  private getMethodT<T>(methodName: string): T {
    return this.controllerClass[methodName] as T;
  }

  private thisBindedInvoke(methodName: string, parameters: any[]) {
    return this.controllerClass[methodName].apply(this.controllerClass, parameters);
  }
}
