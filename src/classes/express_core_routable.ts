import { IRouter } from 'express';
import { BaseCoreRouter, RegistrableMethod, WebDescription } from 'havas-core';
import { ExpressHttpMethod, UniversalPostProcessor } from '../types';
import { ExpressEndpoint, ResultWrapperType } from '../types/classes';
import { ExpressFunction, Middleware, RegistrableMiddleware } from '../types/classes/middleware';
import { ErrorHandlerClass, ErrorHandlerFunction } from './error_handler';
import { ExpressCreator } from './express_creator';

export type MapLike<T> = {
  [name in string | number | symbol]: T;
};

export interface ExpressWebDescription
  extends WebDescription<
    ExpressEndpoint,
    RegistrableMiddleware,
    ResultWrapperType,
    ExpressFunction,
    ErrorHandlerFunction
  > {
  routable: any;
}

/**
 * TODO :: This class should be used as a Data class only representing the
 *         various properties of the endpoint class.
 */
export abstract class ExpressCoreRoutable<T extends IRouter = IRouter>
  extends BaseCoreRouter
  // extends BaseCoreRouter<
  // ExpressEndpoint,
  // RegistrableMiddleware,
  // ErrorHandlerEntry,
  // ResultWrapperType
  // >
  implements ExpressWebDescription
{
  public routable: T;
  public layersInitialized: boolean = false;
  public children: this[] = [];
  public parent: this | undefined;
  public parameterExtractors: { [endpoint_name: string]: ExpressEndpoint['parameters'] } = {};
  public errorHandlerFunctions: ErrorHandlerFunction[] = [];
  public errorHandlerMethods: MapLike<RegistrableMethod<UniversalPostProcessor>> = {};
  public defaultHandlerMethod?: ExpressEndpoint;
  public defaultHandlerFunction?: ExpressFunction;

  protected constructor(routable: T, protected type: 'router' | 'app') {
    super();
    this.routable = routable;
  }

  append(child: ExpressCoreRoutable) {
    return this.addChild(child);
  }

  getRoutable(): T {
    return this.routable;
  }

  public addParameterExtractor(
    methodName: string,
    parameterIndex: number,
    extractorName: string,
    argument?: any[],
    postProcessors?: (((processable: any) => any) | { token: string; args?: any[] })[],
    // postProcessors?: PostProcessorType[],
  ) {
    const extractor = (this.parameterExtractors[methodName] ??= []);

    extractor.push({
      name: extractorName,
      index: parameterIndex,
      arguments: argument,
      postProcessors: postProcessors,
      // : undefined,
      // postProcessors: postProcessors ?? [],
    });
  }

  /**
   * Register request handler methods -- @Get, @Post...
   */
  public registerEndpoint(
    methodName: string,
    methodType: ExpressHttpMethod,
    path: string,
    middlewares: Middleware[],
  ) {
    const endpoint = this.getEndpoint(methodName);
    endpoint.methodType = methodType;
    endpoint.path = path;
    endpoint.middlewares.push(...middlewares);
    endpoint.parameters = this.parameterExtractors[methodName];
  }
  private getEndpoint(name: string): ExpressEndpoint {
    if (this.endpoints[name] === undefined) {
      this.endpoints[name] = {
        postProcessors: [],
        methodName: name,
        middlewares: [],
        parameters: [],
      } as unknown as ExpressEndpoint;
    }

    return this.endpoints[name];
  }

  public add_constructor_middleware(middleware: RegistrableMiddleware) {
    this.middlewares.push(middleware);
  }

  public registerResultWrapperMethod(resultWrapperName: string) {
    this.resultWrapper = {
      methodName: resultWrapperName,
      parameters: this.parameterExtractors[resultWrapperName],
      // postProcessors: {},
    } as RegistrableMethod<UniversalPostProcessor>;
  }

  public registerErrorHandlerFunction(
    errorHandler: ErrorHandlerFunction | ErrorHandlerClass,
  ): void {
    if (typeof errorHandler === 'object') {
      this.errorHandlerFunctions.push((error, request, response, next) =>
        (errorHandler as ErrorHandlerClass).handle({ error, request, response, next }),
      );
    } else if (typeof errorHandler === 'function') {
      this.errorHandlerFunctions.push(errorHandler);
    } else {
      console.error(`Unable to register function with given type: ${typeof errorHandler}`);
      throw new Error('Unable to register function');
    }
  }

  public registerErrorHandler(methodName: string) {
    this.errorHandlerMethods[methodName] = {
      methodName,
      parameters: this.parameterExtractors[methodName],
    };
  }

  public registerDefaultHandlerFunction(defaultHandler: ExpressFunction) {
    this.defaultHandlerFunction = defaultHandler;
  }

  public registerDefaultHandlerMethod(handlerName: string) {
    this.defaultHandlerMethod = {
      methodName: handlerName,
      parameters: this.parameterExtractors[handlerName] ?? [],
      methodType: 'all',
      path: '*',
      middlewares: [],
    };
  }

  /**
   * @deprecated
   */
  public get_initialized_routable(): T {
    return this.getInitializedRoutable();
  }

  public getInitializedRoutable(): T {
    if (!this.layersInitialized) {
      new ExpressCreator(this).setupLayers();
    }

    return this.getRoutable();
  }
}
