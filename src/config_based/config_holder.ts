import { HttpMethodType } from 'havas-core';
import { ProcessorToken } from '../types';
import { ClassExtender, Constructor } from '../util';

export type MapObj<V> = { [name: string]: V };

interface Config {
  parent?: Config;
  path?: string;
  endpoints: MapObj<EndpointDescription>;
}

interface ExctractorReference {
  token: string;
  args: any[];
}

interface EndpointDescription {
  nav?: { method: HttpMethodType; path: string };
  class_method_name: string;
  parameters: {
    extractor: { index: number } & ExctractorReference;
    postProcessors?: ProcessorToken[];
  }[];
}

class ConfigHolder {
  private buffers: { [name: string]: ConfigBuilderBuffer } = {};

  registerClass(_cls: Constructor<any>) {}
  getBuilderBuffer(cls: Constructor<any>): ConfigBuilderBuffer {
    const name = ClassExtender.getClassName(cls);
    return (this.buffers[name] ??= new ConfigBuilderBuffer(cls, name));
  }

  getConfiguration(_cls: Constructor<any>): Config {
    return { endpoints: {} };
  }

  private getNameFromClass() {}
}

class ConfigBuilderBuffer {
  config: Config = {
    endpoints: {},
  };
  constructor(private inner_class: Constructor<unknown>, private class_name: string) {}

  public add_parameter_extractor(
    methodName: string,
    parameterIndex: number,
    extractor: ExctractorReference,
    postProcessors: ProcessorToken[],
  ) {
    const parameters = this.createBaseLine(methodName).parameters;
    parameters.push({
      extractor: { ...extractor, index: parameterIndex },
      postProcessors,
    });
  }

  private createBaseLine(methodName: string) {
    return (this.config.endpoints[methodName] ??= {
      class_method_name: methodName,
      parameters: [],
    });
  }
}

class A {}
