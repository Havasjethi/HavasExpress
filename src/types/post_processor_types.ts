export {
  UniversalPostProcessor,
  ProcessorFunction,
  ProcessorToken,
  ProcessorFactoryToken,
  isProcessorFactory,
  DynamicProcessorFunction,
} from 'havas-core';
