import 'reflect-metadata';
import { App, ExpressRequest, ExpressResponse, Get, MainController } from '../../../../src/index';

@MainController
export class TheMainController extends App {
  @Get('/')
  index(req: ExpressRequest, res: ExpressResponse) {
    res.sendStatus(200);
  }
}
