import 'reflect-metadata';

// Note ::
//  No Need for @Component() since it will be injected with the
//  AsyncContainerModule functionality
export class DummyService {
  constructor(private _statusCode: number = 5) {}

  getStatusCode(): number {
    return this._statusCode;
  }
}
