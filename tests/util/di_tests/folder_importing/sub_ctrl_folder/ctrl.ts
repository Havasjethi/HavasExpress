import { Controller, ExpressRequest, ExpressResponse, Get, Router } from '../../../../../src/index';
import { OtherService } from './other_service';
import { DummyService } from './service';
@Controller()
export class SubController extends Router {
  constructor(private service: DummyService, private otherService: OtherService) {
    super();
  }

  @Get('/sub-endpoint')
  index(req: ExpressRequest, res: ExpressResponse) {
    res.sendStatus(200);
  }

  @Get('/sub-endpoint-service')
  serviceDependent(req: ExpressRequest, res: ExpressResponse) {
    res.sendStatus(this.service.getStatusCode());
  }

  @Get('/sub-endpoint-service-auto')
  getOtherServiceStuff(_: ExpressRequest, res: ExpressResponse) {
    res.sendStatus(this.otherService.getNumber());
  }
}
