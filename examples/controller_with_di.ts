import {
  App,
  ExpressRequest,
  ExpressResponse,
  Get,
  initializeControllers,
  MainController,
} from '../src';

@MainController
class TheMainController extends App {
  @Get('/')
  x(req: ExpressRequest, res: ExpressResponse) {
    res.send('Welcome on the index page!');
  }
}

const main = async () => {
  const mainController = (await initializeControllers({ kind: 'none' }))[0] as App;

  mainController.listen({ listen_options: { port: 3000 } });
};

main();
