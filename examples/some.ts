import {
  App,
  createPostProcessor,
  createPostProcessorFactory,
  ErrorHandler,
  Get,
  MainController,
  Path,
  PathVariable,
  Query,
  ResultWrapper,
  Router,
} from '../src';

export const StringToNumber = createPostProcessor<string, number>((str) => parseInt(str, 10));

export const ValidateType = createPostProcessorFactory((item, type) => {
  console.log(typeof item);
  console.log(item);
  console.log(item === '');
  if (item === '' || typeof item !== type) {
    throw new Error('Types not match!');
  }
  return item;
});

@MainController
@ResultWrapper(({ response }) => response.send('Ok'))
@ErrorHandler((err, req, res, next) => {
  console.log('Problem has been caputed');
  res.send(404);
})
export class ProcessorExample extends App {
  @Get('/:id')
  public methodWithProcessors(@PathVariable('id') id: string) {
    console.log(`Got an Id: ${id}`);
  }

  @Get('/')
  public extractQuery(
    @Query('id', ValidateType('string'))
    id: number,
  ) {
    console.log('Handler for Post method', id);
  }
}

new ProcessorExample().listen({ listen_options: { port: 3000 } });
